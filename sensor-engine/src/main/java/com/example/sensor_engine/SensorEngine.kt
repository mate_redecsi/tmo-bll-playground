package com.example.sensor_engine

import android.util.Log
import com.example.bll_core.TrueMotionSDK

interface SensorEngine {
    fun foo()
}

object SensorEngineImpl: SensorEngine {
    override fun foo() {
        Log.d("SENSORENGINE","sensor engine is up and running -- using a singleton as entry point")
    }
}

fun TrueMotionSDK.installSensorEngine() {
    // init
}

fun TrueMotionSDK.sensorEngine() = SensorEngineImpl

