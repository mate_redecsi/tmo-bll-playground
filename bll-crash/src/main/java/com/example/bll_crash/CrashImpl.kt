package com.example.bll_crash

import android.util.Log
import com.example.sensor_engine.SensorEngine

internal class CrashImpl(private val sensorEngine: SensorEngine) : Crash {
    override fun foo() {
        sensorEngine.foo()
        Log.d("CRASH", "executing Crash module -- using koin")
    }
}
