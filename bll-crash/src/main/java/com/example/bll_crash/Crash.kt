package com.example.bll_crash

import com.example.bll_core.TrueMotionSDK
import com.example.sensor_engine.SensorEngine
import com.example.sensor_engine.installSensorEngine
import com.example.sensor_engine.sensorEngine
import org.koin.core.Koin
import org.koin.core.context.startKoin
import org.koin.dsl.module

interface Crash {
    fun foo()
}

fun TrueMotionSDK.installCrash() {
    TrueMotionSDK.installSensorEngine()

    Injector.koin = startKoin {
        modules(
            module {
                single<SensorEngine> { TrueMotionSDK.sensorEngine() }
                single { CrashImpl(get()) as Crash }
            }
        )
    }.koin
}

fun TrueMotionSDK.crash(): Crash = Injector.koin.get()

private object Injector {
    lateinit var koin: Koin
}
