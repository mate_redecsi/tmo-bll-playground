package com.example.bllsdk

import android.app.Activity
import android.os.Bundle
import com.example.bll_core.TrueMotionSDK
import com.example.bll_crash.crash
import com.example.bll_ubi.ubi

class BLLActivity : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.bll_activity)

        SampleClass(TrueMotionSDK.ubi().tripManager, TrueMotionSDK.crash()).run {
            doSomethingWithUbi()
            doSomethingWithCrashThroughSensorEngine()
        }
    }
}
