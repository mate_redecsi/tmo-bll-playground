package com.example.bllsdk

import android.app.Application
import com.example.bll_core.TrueMotionSDK
import com.example.bll_crash.installCrash
import com.example.bll_ubi.installUBI

class BLLSampleApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        TrueMotionSDK.initialize().apply {
            installUBI()
            installCrash()
        }
    }
}
