package com.example.bllsdk

import com.example.bll_crash.Crash
import com.example.bll_ubi.managers.TripManager

class SampleClass(
    private val tripManager: TripManager,
    private val crash: Crash
) {
    fun doSomethingWithUbi() {
        tripManager.foo()
    }

    fun doSomethingWithCrashThroughSensorEngine() {
        crash.foo()
    }
}
