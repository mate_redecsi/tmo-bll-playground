package com.example.bll_ubi

import com.example.bll_core.TrueMotionSDK
import com.example.bll_ubi.managers.TripManager
import com.example.bll_ubi.managers.TripManagerImpl
import org.kodein.di.DKodein
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

interface UBI {
    val tripManager: TripManager
}

object Injector {
    lateinit var dKodein: DKodein
}

fun TrueMotionSDK.installUBI() {
    Injector.dKodein = Kodein.direct {
            bind<TripManager>() with singleton { TripManagerImpl() }
            bind<UBI>() with provider { UBIImpl(instance()) }
    }
}

fun TrueMotionSDK.ubi(): UBI = Injector.dKodein.instance()

