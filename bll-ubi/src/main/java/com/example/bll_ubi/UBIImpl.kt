package com.example.bll_ubi

import com.example.bll_ubi.managers.TripManager

internal data class UBIImpl(override val tripManager: TripManager): UBI
