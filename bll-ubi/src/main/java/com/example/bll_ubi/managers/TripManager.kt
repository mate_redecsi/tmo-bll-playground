package com.example.bll_ubi.managers

import android.util.Log

interface TripManager {
    fun foo()
}

internal class TripManagerImpl: TripManager {
    override fun foo() {
        Log.d("UBI","UBI is using Kodein")
    }
}
